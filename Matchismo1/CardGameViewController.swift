//
//  CardGameViewController.swift
//  Matchismo1
//
//  Created by Steve Liddle on 9/10/14.
//  Copyright (c) 2014 IS 543. All rights reserved.
//
//  Zachary Lowther
//  Project 1


// ***** For 2 card matching we set gameModeMatchingNumber to 1 because we only want to match one card against another.
// ***** For 3 card matching we set gameModeMatchingNumber to 2 because we want to match two cards against another

// The implementation is working properly. The user is able to switch the game mode for 2 or 3 card matches. 

import UIKit

class CardGameViewController : UIViewController {
    
    @IBOutlet weak var flipLabel: UILabel!
    @IBOutlet weak var cardButton: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet var cardButtons: [UIButton]! {
        didSet {
            game = CardMatchingGame(cardCount: cardButtons.count, deck: PlayingCardDeck())
        }
    }
    
    var game: CardMatchingGame!
    let cardBack = UIImage(named: "CardBack")
    let cardFront = UIImage(named: "CardFront")
    var gameModeMatchingNumber = 1 //Will be 1 by default. We only want to match one card to another one by default
    var flipCount: Int = 0 {
        didSet {
            flipLabel.text = "Flips: \(flipCount)"
        }
    }
    
    
    func updateUI() {
      
        for button in cardButtons {
            let card = game.cardAtIndex(indexOfButton(button))!
            if card.faceUp {
                changeCardToFaceUp(button, card: card)
                button.enabled = !card.unplayable
            } else {
                changeCardToFaceDown(button)
            }
        }
        scoreLabel.text = "Score: \(game.currentScore())"
        if let status = game.getMatchStatus() {
            statusLabel.text = "\(status)"
        }
        
    }
    
    func changeCardToFaceUp(button: UIButton, card: Card) {
        button.setTitle(card.contents, forState: .Normal)
        button.setBackgroundImage(cardFront, forState: .Normal)
    }
    func changeCardToFaceDown(button: UIButton){
        button.setTitle("", forState: .Normal)
        button.setBackgroundImage(cardBack, forState: .Normal)
    }
    func indexOfButton(button: UIButton) -> Int {
        for i in 0 ..< cardButtons.count {
            if button == cardButtons[i] {
                return i
            }
        }
        
        return -1
    }
    
    @IBAction func flipCard(sender: UIButton) {
        game.flipCardAtIndex(indexOfButton(sender))
        ++flipCount
        updateUI()
    }
    
    @IBAction func dealCards(sender: UIButton) {
        
        self.flipCount = 0
        self.statusLabel.text = "Card Flipping Results"
        self.scoreLabel.text = "Score: 0"
        game = CardMatchingGame(cardCount: cardButtons.count, deck: PlayingCardDeck())
        game.changeMatchingCount(gameModeMatchingNumber) //Make sure that when the deck is re-dealt that you set the correct matching number
        for button in cardButtons {
            button.enabled = true
            changeCardToFaceDown(button)
        }
    }
       
}