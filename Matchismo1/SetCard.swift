//
//  SetCard.swift
//  Matchismo1
//
//  Created by Zachary Lowther on 10/1/14.
//  Copyright (c) 2014 IS 543. All rights reserved.
//

import Foundation
class SetCard: Card {
    let SAME_SCORE = 2
    let DIFFERENT_SCORE = 4
    var number: Int?
    var shape: String?
    var shading: String?
    var color: String?


    var attributedContents: NSAttributedString?
    
    override init() {
        number = nil
        shape = nil
        shading = nil
        color = nil
        attributedContents = nil
    }
    
    override func match(otherCards: [Card]) -> Int {
        var score = 0
        var validSet = false
        if otherCards.count == 3 {
            var numberArray: [Int] = []
            var shapeArray: [String] = []
            var shadingArray: [String] = []
            var colorArray: [String] = []
            
            for i in 0 ..< otherCards.count {
                numberArray.insert((otherCards[i] as SetCard).number!, atIndex: i)
                shapeArray.insert((otherCards[i] as SetCard).shape!, atIndex: i)
                shadingArray.insert((otherCards[i] as SetCard).shading!, atIndex: i)
                colorArray.insert((otherCards[i] as SetCard).color!, atIndex: i)
            }
            
            if numberArray[0] == numberArray[1] && numberArray[1] == numberArray[2]{
                validSet = true
                score += SAME_SCORE
                
            }else if numberArray[0] != numberArray[1] && numberArray[1] != numberArray[2]
                && numberArray[0] != numberArray[2] {
                validSet == true
                score += DIFFERENT_SCORE
            }else {
                return -4
            }
            
            if shapeArray[0] == shapeArray[1] && shapeArray[1] == shapeArray[2]{
                validSet = true
                score += SAME_SCORE
            }else if shapeArray[0] != shapeArray[1] && shapeArray[1] != shapeArray[2]
                && shapeArray[0] != shapeArray[2] {
                    validSet == true
                    score += DIFFERENT_SCORE
            }else {
                return -4
            }
            
            if shadingArray[0] == shadingArray[1] && shadingArray[1] == shadingArray[2]{
                validSet = true
                score += SAME_SCORE
            }else if shadingArray[0] != shadingArray[1] && shadingArray[1] != shadingArray[2]
                && shadingArray[0] != shadingArray[2] {
                    validSet == true
                    score += DIFFERENT_SCORE
            }else {
                return -4
            }
            
            if colorArray[0] == colorArray[1] && colorArray[1] == colorArray[2]{
                validSet = true
                score += SAME_SCORE
            }else if colorArray[0] != colorArray[1] && colorArray[1] != colorArray[2]
                && colorArray[0] != colorArray[2] {
                    validSet == true
                    score += DIFFERENT_SCORE
            }else {
                return -4
            }
          
        }
        if validSet {
            return score
        }else {
            return -4
        }
        
    }
    
    class func validNumbers() -> [Int] {
        return [1,2,3]
    }
    class func validColors() -> [String] {
        return ["Red", "Green", "Purple"]
    }
    class func validShapes() -> [String] {
        return ["▲", "●", "■"]
    }
    class func validShading() -> [String] {
        return ["Filled", "Striped", "Open"]
    }
}
