//
//  SetDeck.swift
//  Matchismo1
//
//  Created by Zachary Lowther on 10/1/14.
//  Copyright (c) 2014 IS 543. All rights reserved.
//

import Foundation
class SetDeck: Deck {
    
    override init(){
        super.init()
        
        generateAllPossibleCards()
        
    }
    
    func generateAllPossibleCards() {
        for number in SetCard.validNumbers() {
            for color in SetCard.validColors() {
                for shape in SetCard.validShapes() {
                    for shading in SetCard.validShading() {
                        var card = SetCard()
                        card.number = number
                        card.color = color
                        card.shading = shading
                        card.shape = shape
                        addCard(card, atTop: true)
                    }
                }
            }
        }
    }
}
