//
//  PlayingCard.swift
//  Matchismo1
//
//  Created by Zachary Lowther on 9/11/14.
//  Copyright (c) 2014 IS 543. All rights reserved.
//

import Foundation

class PlayingCard: Card {
    let RANK_MATCH_SCORE = 4
    let SUIT_MATCH_SCORE = 1
    var suit: String! {
        didSet {
            if !contains(PlayingCard.validSuits(), suit) {
                suit = "?"
            }
            contents = "\(PlayingCard.rankStrings()[rank])\(suit)"
        }
    }
    
    var rank: Int! {
        didSet {
            if rank < 0 || rank > PlayingCard.maxRank() {
                rank = 0
            }
            contents = "\(PlayingCard.rankStrings()[rank])\(suit)"
        }
    }
    
    class func validSuits() -> [String] {
        return ["♥", "♦", "♠", "♣"]
    }
    private class func rankStrings() -> [String] {
        return ["?", "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
    }
    class func maxRank() -> Int {
        return rankStrings().count - 1
    }
    override func match(otherCards: [Card]) -> Int {
        var score = 0
        if otherCards.count == 1 {
            if let otherCard = otherCards.last as? PlayingCard {
                if otherCard.suit == suit {
                    score = SUIT_MATCH_SCORE
                } else if otherCard.rank == rank {
                    score = RANK_MATCH_SCORE
                }
            }
        }else if otherCards.count == 2 {
            if let firstCard = otherCards.first as? PlayingCard {
                /*
                 * My formula: 4/2 = x/3. x would be 6
                 *  and 1/2 = x/3 = 1.5 or rounded to 2. 
                 * So we use 6 for rank matches and 2 for suit matches based upon
                 * previous scoring system of two card matches
                 */
                if let secondCard = otherCards.last as? PlayingCard {
                    if firstCard.suit == secondCard.suit && firstCard.suit == suit {
                        score = SUIT_MATCH_SCORE * 2 //SUIT_MATCH_SCORE should be 2
                    }else if firstCard.rank == secondCard.rank && firstCard.rank == rank {
                        score = RANK_MATCH_SCORE * 2 //RANK_MATCH_SCORE should be 6
                        //Had to change 1.5 to 2 in xcode 6.1 for FloatConversion Problem. Was working in xcode 6.0.1
                    }
                }
               
            }
        }
        return score
    }
    
}