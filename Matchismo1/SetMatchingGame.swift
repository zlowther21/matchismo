//
//  SetMatchingGame.swift
//  Matchismo1
//
//  Created by Zachary Lowther on 10/1/14.
//  Copyright (c) 2014 IS 543. All rights reserved.
//

import Foundation
class SetMatchingGame: SetCard {
    private lazy var cards: [Card] = []
    private lazy var cardsToMatch: [Card] = []
    private var score = 0
    let FLIP_COST = 1
    var matchStatus:NSMutableAttributedString?
    var prefix:NSMutableAttributedString?
    var and = NSMutableAttributedString(string: " & ")
    
    init(cardCount: Int, deck: Deck) {
        super.init()
        for i in 0 ..< cardCount {
            if let card = deck.drawRandomCard() {
                cards.append(card)
            }
        }
        self.score = 0
        self.matchStatus = nil
        self.prefix = nil
    }
    
    func getRandomCardCollection() -> [Card] {
        return self.cards
    }
    
    func getCardAtIndex(index: Int){
        //Must get mutable copies of NSAttributedString instead of referencing NSMutableAttributedStrings
        
        if let card = cardAtIndex(index) as? SetCard {
            card.selected = true
            prefix = NSMutableAttributedString(string: "Selected Card: ")
            prefix!.appendAttributedString(card.attributedContents!.mutableCopy() as NSMutableAttributedString)
            matchStatus = prefix
            if cardsToMatch.count < 2 {
                cardsToMatch.append(card)
            }else {
                cardsToMatch.append(card)
                let matchScore = match(cardsToMatch)
                
                
                if matchScore > 0 {
                    prefix = NSMutableAttributedString(string: "Matched ")
                    prefix!.appendAttributedString((cardsToMatch[0] as SetCard).attributedContents!.mutableCopy() as NSMutableAttributedString)
                    for i in 1 ..< cardsToMatch.count {
                        if !cardsToMatch[i].faceUp && !cardsToMatch[i].unplayable {
                            cardsToMatch[i].unplayable = true
                            cardsToMatch[i].faceUp = true
                            prefix!.appendAttributedString(and)
                            prefix!.appendAttributedString((cardsToMatch[i] as SetCard).attributedContents!)
                        }
                    }
                    cardsToMatch[0].unplayable = true //Must disable the first card too
                    cardsToMatch[0].faceUp = true
                    
                    var tempAttString = NSMutableAttributedString(string: " for \(matchScore) points")
                    prefix!.appendAttributedString(tempAttString)
                    
                    matchStatus = prefix //Save whole status string

                }else {
                    prefix = (cardsToMatch[0] as SetCard).attributedContents!.mutableCopy() as? NSMutableAttributedString
                    for i  in 1 ..< cardsToMatch.count {
                        if !cardsToMatch[i].faceUp && !cardsToMatch[i].unplayable {
                            prefix!.appendAttributedString(and)
                            prefix!.appendAttributedString((cardsToMatch[i] as SetCard).attributedContents!.mutableCopy() as NSMutableAttributedString)
                            cardsToMatch[i].selected = false
                            
                        }
                    }
                    cardsToMatch[0].selected = false //Must consider first card
                    var tempAttString = NSMutableAttributedString(string: " don't match: \(matchScore) point penalty.")
                    prefix!.appendAttributedString(tempAttString)
                    matchStatus = prefix //Save whole status string
                }
                
                score += matchScore
                
               
                
                cardsToMatch.removeAll(keepCapacity: false)
            }
            // always charge a cost to flip
            score -= FLIP_COST
        }
    }
    func cardAtIndex(index: Int) -> Card? {
        if index >= 0 && index < cards.count {
            return cards[index]
        }
        return nil
    }
    func currentScore() -> Int {
        return score
    }
    func getMatchStatus() -> NSMutableAttributedString?
    {
        return matchStatus
    }
  
    
}