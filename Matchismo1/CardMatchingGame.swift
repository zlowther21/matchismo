//
//  CardMatchingGame.swift
//  Matchismo1
//
//  Created by Zachary Lowther on 9/22/14.
//  Copyright (c) 2014 IS 543. All rights reserved.
//


// *** SEE BOTTOM FOR CARD MATCHING RESULTS STRING EXPLANATION ***

import Foundation
class CardMatchingGame: PlayingCard{
    private lazy var cards: [Card] = []
    private lazy var cardsToMatch: [Card] = []
    private var score = 0
    private var matchStatus: String?
    private var matchingCount: Int = 1 //Default is to only match one card
    let FLIP_COST = 1
    let MATCH_BONUS = 4
    let MISMATCH_PENALTY = 2

    
    init(cardCount: Int, deck: Deck) {
        super.init()
        for i in 0 ..< cardCount {
            if let card = deck.drawRandomCard() {
                cards.append(card)
            }
        }
        self.score = 0
        self.matchStatus = nil
    }

    
    func changeMatchingCount(count: Int){
        self.matchingCount = count
    }
    
    func flipCardAtIndex(index: Int) {
        if let card = cardAtIndex(index) {
            if !card.unplayable {
                if !card.faceUp {
                    matchStatus = "Flipped up \(card.contents)"
                    var counter: Int = 0
                    for otherCard in cards {
                        if otherCard.faceUp && !otherCard.unplayable {
                            if counter == self.matchingCount {
                                break
                            }
                            
                            counter++
                            cardsToMatch.append(otherCard)
                            
                        }
                    }
                    if cardsToMatch.count > 0 && cardsToMatch.count == self.matchingCount{
                        let matchScore = card.match(cardsToMatch)
                        // match tells how good a match it is, or 0 if mismatch
                        if matchScore > 0 {
                            var tempString:String = "Matched \(card.contents) "
                            for otherCard in cardsToMatch {
                                if otherCard.faceUp && !otherCard.unplayable {
                                    otherCard.unplayable = true
                                    tempString += "& \(otherCard.contents) " //building matchStatus string
                                }
                            }
                            tempString += "for \(matchScore * MATCH_BONUS) points"
                            
                            card.unplayable = true
                            score += matchScore * MATCH_BONUS
                            matchStatus = tempString //Save whole status string
                            
                        } else {
                            score -= MISMATCH_PENALTY
                            var tempString:String = "\(card.contents) "
                            for otherCard in cardsToMatch {
                                if otherCard.faceUp && !otherCard.unplayable {
                                    tempString += "& \(otherCard.contents) " //building matchStatus string
                                }
                            }
                            tempString += "don't match: \(MISMATCH_PENALTY) point penalty."

                            matchStatus = tempString //Save whole status string
                        }
                       
                    }
                    cardsToMatch.removeAll(keepCapacity: false) //Empty all cards so that it will match correctly
                    
                    // always charge a cost to flip
                    score -= FLIP_COST
                }
                card.faceUp = !card.faceUp
            }
        }
    }
    func cardAtIndex(index: Int) -> Card? {
        if index >= 0 && index < cards.count {
            return cards[index]
        }
        return nil
    }
    func currentScore() -> Int {
        return score
    }
    func getMatchStatus() -> String? {
        /*
         * I decide to build the card flipping results strings in this class/model. The 
         * strings are built in the flipCardAtIndex function and stored. In
         * the controller I call this method to access those formulated strings. I think 
         * that the model should be responsible for describing itself and its data. However,
         * when the string needs to be displayed, the controller should call this model
         * method and then send its results to the UI. I feel that this pattern follows
         * MVC.
         */
        return matchStatus
    }
    
    
    
}
