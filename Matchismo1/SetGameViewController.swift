//
//  SetGameViewController.swift
//  Matchismo1
//
//  Created by Zachary Lowther on 10/1/14.
//  Copyright (c) 2014 IS 543. All rights reserved.
//

import UIKit
class SetGameViewController: UIViewController {

    var game: SetMatchingGame!
    var selectionCounter:Int = 0
    var flipCount: Int = 0 {
        didSet {
            self.flipLabel.text = "Flips: \(self.flipCount)"
        }
    }
    
    @IBOutlet weak var flipLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet var cardButtons: [UIButton]! {
        didSet {
            initializeCards()
        }
    }
    func initializeCards(){
        game = SetMatchingGame(cardCount: cardButtons.count, deck: SetDeck())
        var randomCards = game.getRandomCardCollection()
        for i in 0 ..< randomCards.count {
            addCardContents(randomCards[i] as SetCard, button: cardButtons[i])
            //randomCards and cardButtons should always be the same length
        }

    }
    func addCardContents(card: SetCard, button: UIButton) {
    
        var number:Int! = card.number
        var color:String! = card.color
        var shape:String! = card.shape
        var shading:String! = card.shading
        var shapeColor:UIColor!
        var shadingColor:UIColor!
        
        if number == 2 {
            shape = shape + shape
        }else if number == 3 {
            shape = shape + shape + shape
        }
        
        switch color {
        case "Purple":
            shapeColor = UIColor.purpleColor()
        case "Green":
            shapeColor = UIColor.greenColor()
        default:
            shapeColor = UIColor.redColor()
        }
        
        switch shading {
        case "Filled":
            shadingColor = shapeColor.colorWithAlphaComponent(1.0)
        case "Striped":
            shadingColor = shapeColor.colorWithAlphaComponent(0.2)
        default:
            shadingColor = shapeColor.colorWithAlphaComponent(1.0)
            
        }
     
        var fontSize:CGFloat = 16.0
        if shape.hasPrefix("●") {
            fontSize = 28.0
        }
        let systemFont = UIFont.systemFontOfSize(fontSize) //Had to lower font due to lack of space
        
        var attributedString = NSMutableAttributedString(string: shape)
        var range = NSMakeRange(0, attributedString.length)
        attributedString.addAttribute(NSFontAttributeName, value: systemFont, range: range)
        attributedString.addAttribute(NSForegroundColorAttributeName, value:shadingColor , range: range )
        attributedString.addAttribute(NSStrokeColorAttributeName, value: shapeColor, range:range)

        if shading == "Open" {
            attributedString.addAttribute(NSStrokeWidthAttributeName, value: 5.0, range: range)
        }else {
            attributedString.addAttribute(NSStrokeWidthAttributeName, value: -5.0, range: range)
   
        }

        
        //Necessary Updates for the card and button
        card.attributedContents = attributedString
        
        card.faceUp = false
        card.unplayable = false
        
        button.setAttributedTitle(attributedString, forState: .Normal)
        reenableCard(button)
        
    
        
    
        
    }
    func indexOfButton(button: UIButton) -> Int {
        for i in 0 ..< cardButtons.count {
            if button == cardButtons[i] {
                return i
            }
        }
        
        return -1
    }
    func disableCard(button: UIButton){
        button.alpha = 0.0
    }
    func reenableCard(button: UIButton){
        button.alpha = 1.0
        button.enabled = true
    }
    
    func updateUI() {
        for button in cardButtons {
            let card = game.cardAtIndex(indexOfButton(button))!
            
            if card.selected {
                button.alpha = 0.8 //If a card is clicked, it alphas
            }else {
                button.alpha = 1.0
            }
            
            if card.faceUp {
                disableCard(button)
                button.enabled = false
            }
         
        }
        scoreLabel.text = "Score: \(game.currentScore())"
        if let status = game.getMatchStatus() {
            statusLabel.attributedText = status
        }

    }
    @IBAction func dealCards(sender: UIButton) {
        initializeCards()
        updateUI()
        flipCount = 0
        statusLabel.text = "Set Matching Results"
    }
    
    @IBAction func selectCard(sender: UIButton) {
        game.getCardAtIndex(indexOfButton(sender))
        ++flipCount
        updateUI()
    }
    
}
